<?php

namespace App\Http\Controllers;

use App\QuakerQuote;
use Illuminate\Http\Request;

class QuakerQuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QuakerQuote  $quakerQuote
     * @return \Illuminate\Http\Response
     */
    public function show(QuakerQuote $quakerQuote)
    {
        return view('layouts.footer', ['quakerQuote' => $quakerQuote]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QuakerQuote  $quakerQuote
     * @return \Illuminate\Http\Response
     */
    public function edit(QuakerQuote $quakerQuote)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QuakerQuote  $quakerQuote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuakerQuote $quakerQuote)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QuakerQuote  $quakerQuote
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuakerQuote $quakerQuote)
    {
        //
    }

    public function getRandomQuote() {
        $quakerQuote = QuakerQuote::find(mt_rand(1, QuakerQuote::count()));
        return view('layouts.footer', ['quakerQuote' => $quakerQuote]);
    }
}
