<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function homePage() {
        return view('welcome');
    }

    public function servicesPage() {
        return view('services');
    }

    public function silentMeetingPage() {
        return view('silentmeeting');
    }

    public function contactPage() {
        return view('contact');
    }

    public function ministriesPage() {
        return view('ministries');
    }

    public function getPosters() {
        return view('resources.posters');
    }
}
