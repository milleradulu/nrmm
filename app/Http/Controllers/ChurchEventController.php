<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\ChurchEvent;
use Illuminate\Http\Request;

class ChurchEventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tevents = ChurchEvent::where('start_date', Carbon::today())->orderBy('start_date', 'asc')->paginate(8);
        $uevents = ChurchEvent::where('start_date', '>', Carbon::today())->orderBy('start_date', 'asc')->paginate(8);
        $pevents = ChurchEvent::where('start_date', '<', Carbon::today())->orderBy('start_date', 'desc')->paginate(8);

        return view('events', ['tevents' => $tevents, 'uevents' => $uevents, 'pevents' => $pevents]);
    }

    public function gallery() {
        $events = ChurchEvent::where('start_date', '<', Carbon::today())->orderBy('start_date', 'desc')->simplePaginate(5);

        return view('gallery', ['events' => $events]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ChurchEvent  $event
     * @return \Illuminate\Http\Response
     */
    public function show(ChurchEvent $event)
    {
        $event->start_date = Carbon::createFromFormat('Y-m-d', $event->start_date);
        $event->end_date = Carbon::createFromFormat('Y-m-d', $event->end_date);
        $event->start_time = Carbon::createFromTimeString($event->start_time);
        $event->end_time = Carbon::createFromTimeString($event->end_time);
        return view('event', ['event' => $event]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChurchEvent  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(ChurchEvent $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ChurchEvent  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChurchEvent $event)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChurchEvent  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChurchEvent $event)
    {
        //
    }

    /**
     * Display the gallery resource.
     *
     * @param  \App\ChurchEvent  $event
     * @return \Illuminate\Http\Response
     */
    public function eventGallery(ChurchEvent $gallery) {
        $gallery->start_date = Carbon::createFromFormat('Y-m-d', $gallery->start_date);
        $gallery->end_date = Carbon::createFromFormat('Y-m-d', $gallery->end_date);
        $gallery->start_time = Carbon::createFromTimeString($gallery->start_time);
        $gallery->end_time = Carbon::createFromTimeString($gallery->end_time);
        return view('eventgallery', ['gallery' => $gallery]);
    }
}
