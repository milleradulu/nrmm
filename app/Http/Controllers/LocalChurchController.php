<?php

namespace App\Http\Controllers;

use App\LocalChurch;
use Illuminate\Http\Request;

class LocalChurchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meetings = LocalChurch::all();
        return view('localmeetings', ['meetings' => $meetings]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LocalChurch  $localChurch
     * @return \Illuminate\Http\Response
     */
    public function show(LocalChurch $localChurch)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LocalChurch  $localChurch
     * @return \Illuminate\Http\Response
     */
    public function edit(LocalChurch $localChurch)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LocalChurch  $localChurch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LocalChurch $localChurch)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LocalChurch  $localChurch
     * @return \Illuminate\Http\Response
     */
    public function destroy(LocalChurch $localChurch)
    {
        //
    }
}
