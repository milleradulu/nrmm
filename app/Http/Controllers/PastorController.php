<?php

namespace App\Http\Controllers;

use App\LocalChurch;
use App\Pastor;
use Illuminate\Http\Request;

class PastorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pastors = Pastor::where('active', '0')->get();

        $stations = LocalChurch::all();

        $pastorsWithStations = [];

        foreach($pastors as $pastor) {
            foreach($stations as $station) {
                if($pastor->local_church_id == $station->id) $pastor->local_church_id = $station->name;
            }
            array_push($pastorsWithStations, $pastor);
        }
        return view('pastors', ['pastors' => $pastorsWithStations]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pastor  $pastor
     * @return \Illuminate\Http\Response
     */
    public function show(Pastor $pastor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pastor  $pastor
     * @return \Illuminate\Http\Response
     */
    public function edit(Pastor $pastor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pastor  $pastor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pastor $pastor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pastor  $pastor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pastor $pastor)
    {
        //
    }
}
