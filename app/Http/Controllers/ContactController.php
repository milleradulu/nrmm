<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests\SaveMessage;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contact');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SaveMessage  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveMessage $request)
    {
        $data = $request->validated();
        
        $message = new Contact;
        $message->fill($data);
        
        $message->save();
        
        return back()->with('message', 'Your message has been received');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(SaveMessage $request, Contact $contact)
    {
        $data = $request->validated();

        $message = Contact::findOrFail($contact->id);
        $message->fill($data);
        $message->save();

        return $message;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        return Contact::destroy($contact);
    }
}
