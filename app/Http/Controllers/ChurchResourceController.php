<?php

namespace App\Http\Controllers;

use App\ChurchResource;
use Illuminate\Http\Request;

class ChurchResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ChurchResource  $churchResource
     * @return \Illuminate\Http\Response
     */
    public function show(ChurchResource $churchResource)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChurchResource  $churchResource
     * @return \Illuminate\Http\Response
     */
    public function edit(ChurchResource $churchResource)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ChurchResource  $churchResource
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChurchResource $churchResource)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChurchResource  $churchResource
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChurchResource $churchResource)
    {
        //
    }
}
