<?php

namespace App\Http\ViewComposers;

use App\QuakerQuote;
use Illuminate\View\View;

class QuoteComposer
{
    public $quakerQuote;

    /**
     * Create a quote composer.
     *
     * @return void
     */
    public function __construct()
    {
        $this->quakerQuote = QuakerQuote::count() > 0 ? QuakerQuote::find(mt_rand(1, QuakerQuote::count())): null;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('quakerQuote', $this->quakerQuote);
    }
}