@extends('layouts.app') 
@section('title', 'Pastors') 
@section('content')
<div>
    <section id="inner-headline" style="margin-top:5px !important;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle w3-center">Pastors</h2>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container">
            <section class="features">
                <div class="container">
                    <div class="row w3-center">

                        @foreach ($pastors as $pastor)
                        <div class='col-md-4 w3-center'>
                            <img src='@if($pastor->gender == 0) {{ asset('images/leaders/male.jpg') }} @else {{ asset('images/leaders/female.jpg
                                ') }} @endif' style='height:300px !important' class='img-rounded'>
                            <p style='margin-top:10px;'>Pst {{ $pastor->first_name }} {{ $pastor->last_name}}</p>
                            <p style='margin-top:10px;'>Station: {{ $pastor->local_church_id }}</p>
                        </div>
                        @endforeach

                    </div>
                </div>
            </section>
        </div>
    </section>
</div>
@endsection