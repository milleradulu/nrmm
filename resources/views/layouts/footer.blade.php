<footer class="w3-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="widget">
                    <h5 class="widgetheading">Our Contacts</h5> <address> <strong>Friends International Centre</strong><br>Ngong
                            Road, Near Uchumi Hyper<br>Nairobi, Kenya </address>
                    <p> <a href="https://www.facebook.com/ngongroadmonthlymeeting/" data-placement="top" title="Facebook" target="_blank"><i class="fa fa-facebook fa-3x"></i></a></li>
                        <a href="https://twitter.com/ngongroadmm" data-placement="top" title="Twitter" style="margin-left:10px; margin-right:10px;"><i class="fa fa-twitter fa-3x" target="_blank"></i></a>
                        </li><a href="https://www.instagram.com/ngongroadmonthly/" data-placement="top" title="Instagram"
                            target="_blank"><i class="fa fa-instagram fa-3x"></i></a></li>
                    </p>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="widget">
                    <h5 class="widgetheading">Quick Links</h5>
                    <ul class="link-list">
                        <li><a href="/events">Events</a></li>
                        <li><a href="/pastors">Pastors </a></li>
                        <li><a href="/gallery">Gallery</a></li>
                        <li><a href="/contact">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="widget">
                    <h5 class="widgetheading">Leadership</h5>
                    <ul class="link-list">
                        <li><a href="/leadership#main"> Main </a></li>
                        <li><a href="/leadership#quakermen"> Quaker Men </a></li>
                        <li><a href="/leadership#usfw"> USFW </a></li>
                        <li><a href="/leadership#youth"> Youth </a></li>
                        <li><a href="/leadership#scs"> Sunday School </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="widget">
                    <h5 class="widgetheading">Ministries</h5>
                    <ul class="link-list">
                        <li><a href="/ministries#fcdi"> FCDI </a></li>
                        <li><a href="/silentmeeting"> Silent Meeting </a></li>
                        <li><a href="/ministries#praise"> Praise & Worship </a></li>
                        <li><a href="/ministries#outreach"> Outreach & Missions </a></li>
                        <li><a href="/ministries#catechism"> Catechism Classes </a></li>
                        <li><a href="/ministries#mercy"> Mercy Ministry </a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div style="font-size:15px; color: white !important;">
                <h3> Your dose of Quaker Quotes </h3>
                @if ($quakerQuote != null)
                <blockquote style="font-size:20px; color:white !important;">
                        <i class="fa fa-quote-left fa-2x w3-margin"></i>{{ $quakerQuote->quote }}<i class="fa fa-quote-right fa-2x w3-margin"></i><br>
                        <cite style="font-size:15px; color:white;">{{ $quakerQuote->author }} --> {{ $quakerQuote->year }}</cite></blockquote>
                @else
                <blockquote style="font-size:20px; color:white !important;">There are no available quotes at this moment.</blockquote>
                @endif
            </div>
        </div>
    </div>
    <div id="sub-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright">
                        <p class="w3-center"> <span>&copy; Design by: </span><a href="https://www.gigaberry.com" target="_blank">Gigaberry
                                    |</a> <span>Made with love by: </span><a href="https://milleradulu.co.ke" target="_blank">Miller
                                    Adulu</a> </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>