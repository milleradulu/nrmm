<header>
    <div class="navbar"></div>
    <div class="navbar-wrapper">
        <div class="container-fluid">
            <nav class="navbar navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header"> <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                            aria-controls="navbar" style="margin-top:2px !important;">
                                <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                                <span class="icon-bar"></span> </button> <a class="navbar-brand" href="/"><img src="{{ asset('images/logo.png') }}"
                                    alt="logo" /></a> </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li @if(Route::current()->uri == '/') class="active" @endif><a href="/" style="font-size:18px;">Home</a></li>
                            <li class="dropdown"><a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true"
                                    aria-expanded="false" style="font-size:18px;">Church
                                        Programmes <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/services" style="font-size:18px;">Services</a></li>
                                    <li><a href="/silentmeeting" style="font-size:18px;">Silent Meeting</a></li>
                                    <li><a href="/leadership" style="font-size:18px;">Leadership</a></li>
                                    <li><a href="/ministries" style="font-size:18px;">Ministries</a></li>
                                    <li><a href="/localmeetings" style="font-size:18px;">Local Meetings</a></li>
                                </ul>
                            </li>
                            <li @if(Route::current()->uri == 'pastors') class="active" @endif><a href="/pastors" style="font-size:18px;">Pastors</a></li>
                            <li @if(Route::current()->uri == 'events') class="active" @endif><a href="/events" style="font-size:18px;">Events</a></li>
                            <li @if(Route::current()->uri == 'gallery') class="active" @endif><a href="/gallery" style="font-size:18px;">Gallery</a></li>
                            <li @if(Route::current()->uri == 'contact') class="active" @endif><a href="/contact" style="font-size:18px;">Contact us</a></li>
                            {{-- <li class="dropdown"><a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true"
                                    aria-expanded="false" style="font-size:18px;">Resources<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/posters" style="font-size:18px;">Posters</a></li>
                                </ul>
                            </li> --}}
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <section id="inner-headline" style="margin-top:0px !important;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="pageTitle w3-center">Friends Church Quakers | Ngong Road Monthly Meeting</h2>
                    </div>
                </div>
            </div>
        </section>
    </div>
</header>