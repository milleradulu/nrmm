@extends('layouts.app') 
@section('title', $gallery->name . ' gallery') 
@section('content')
<section id="inner-headline" style="margin-top:5px !important;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle">{{ $gallery->name }}</h2>
                </div>
            </div>
        </div>

        {{ $gallery }}
    </section>
@endsection