@extends('layouts.app') 
@section('title', 'Local Meetings') 
@section('content') @foreach ($meetings as $meeting)
<div id="{{ $meeting->name }}">
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle w3-center">{{ $meeting->name }}</h2>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container">
            <section class="features">
                <div class="container">
                    <div class="row w3-centr">
                    <h3>Location: {{ $meeting->location }}</h3>
                        {!! $meeting->history !!}
                    </div>
                </div>
            </section>
        </div>
    </section>
</div>
@endforeach
@endsection