@extends('layouts.app') 
@section('title', 'Silent Meeting') 
@section('content')
<section id="inner-headline" style="margin-top:5px !important;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="pageTitle" style="text-align:center">Silent Meeting</h2>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <div class="container">
        <div class="row w3-center"> <img class="img-responsive" src="images/silentmeeting.jpg">
            <p>
                <h3>What is the Silent Meeting?</h3> The Silent Meeting is an special <b>un-programmed</b> meeting for worship
                based on the original method for worship initiated by the founder of the Religious Society of Friends, Quakers
                - George Fox. To honour this history and way of worship, the silent meeting has been maintained with un-programmed
                worship every Sunday. This method of worship is widely recognised in the International sphere of Quakers
                more often sought after from Quakers who have been living outside of Kenya.
            </p>
            <p>
                <h3>How is the Silent Meting conducted?</h3> As the name suggests, the Silent Meeting is conducted in silence.
                Everyone sits in circular formation sharing the silence for personal reflection in a communal space. The
                meeting may not always be in total silence; when the urge finds them, the silent space is provided for people
                to feel encouraged to testimony their reflection. If one feels called to testimony, they stand up and share
                their findings out loud breaking the silence for that moment without coercion.
            </p>
            <p>
                <h3>When is the silent meeting?</h3> The Silent Meeting is conducted every <a id="impLink" href="https://calendar.google.com/calendar/event?action=TEMPLATE&tmeid=MjJzMHA5Mmtka2NxNmVyOGprdWM2dmZoZnNfMjAxNzA2MjVUMDczMDAwWiBuZ29uZ3JvYWRtb250aGx5QG0&tmsrc=ngongroadmonthly%40gmail.com">Sunday
                        from 10:30am to 11:30am </a> on the grounds of the <a id="impLink" href="https://www.google.com/maps/place/Friends+International+Centre,+Ngong+Rd,+Nairobi+City,+Kenya/@-1.3005103,36.7871189,17z/data=!3m1!4b1!4m5!3m4!1s0x182f109bc4842189:0xd973a345ed0893e1!8m2!3d-1.3004682!4d36.7891878">Friends
                        International Centre, Ngong Road, Nairobi</a>. It is advised to arrive slightly earlier so as not
                to disturb the shared silence. The Silent Meeting is part of the Ngong Road Monthly Meeting. There is no
                other silent meeting in Kenya.
            </p>
        </div>
    </div>
</section>
@endsection