@extends('layouts.app') 
@section('title', 'Leadership') 
@section('content')
<div id="main">
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle w3-center">Main Meeting</h2>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container">
            <section class="features">
                <div class="container">
                    <div class="row w3-center">
                        @foreach ($mains as $main)
                        <div class='col-md-3 w3-center'>
                            <img src='@if($main->gender == 0) {{ asset('images/leaders/male.jpg') }} @else {{ asset('images/leaders/female.jpg
                            ') }} @endif' height='300px' width='300px' class='img-rounded'><br>
                            <p style="margin-top:10px;"><b> {{ $main->first_name }} {{ $main->last_name }} </b></p style=".'margin-top:10px;'."><br>
                            <p><i>Post: {{ $main->post }}</i></p><br>
                        </div>
                        @endforeach
                    </div>
                </div>
            </section>
        </div>
    </section>
</div>
<div id="quakermen">
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle w3-center">Quaker Men</h2>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container">
            <section class="features">
                <div class="container">
                    <div class="row w3-center">
                        @foreach ($quakermen as $quakerman)
                        <div class='col-md-3 w3-center'>
                            <img src='@if($quakerman->gender == 0) {{ asset('images/leaders/male.jpg') }} @else {{ asset('images/leaders/female.jpg
                            ') }} @endif' height='300px' width='300px' class='img-rounded'><br>
                            <p style="margin-top:10px;"><b> {{ $quakerman->first_name }} {{ $quakerman->last_name }} </b></p style=".'margin-top:10px;'."><br>
                            <p><i>Post: {{ $quakerman->post }}</i></p><br>
                        </div>
                        @endforeach
                    </div>
                </div>
            </section>
        </div>
    </section>
</div>
<div id="usfw">
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle w3-center">United Society of Friends Women (USFW)</h2>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container">
            <section class="features">
                <div class="container">
                    <div class="row w3-center">
                        @foreach ($usfws as $usfw)
                        <div class='col-md-3 w3-center'>
                            <img src='@if($usfw->gender == 0) {{ asset('images/leaders/male.jpg') }} @else {{ asset('images/leaders/female.jpg
                            ') }} @endif' height='300px' width='300px' class='img-rounded'><br>
                            <p style="margin-top:10px;"><b> {{ $usfw->first_name }} {{ $usfw->last_name }} </b></p style=".'margin-top:10px;'."><br>
                            <p><i>Post: {{ $usfw->post }}</i></p><br>
                        </div>
                        @endforeach
                    </div>
                </div>
            </section>
        </div>
    </section>
</div>
<div id="youth">
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle w3-center">Youth</h2>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container">
            <section class="features">
                <div class="container">
                    <div class="row w3-center">
                        @foreach ($youths as $youth)
                        <div class='col-md-3 w3-center'>
                            <img src='@if($youth->gender == 0) {{ asset('images/leaders/male.jpg') }} @else {{ asset('images/leaders/female.jpg
                            ') }} @endif' height='300px' width='300px' class='img-rounded'><br>
                            <p style="margin-top:10px;"><b> {{ $youth->first_name }} {{ $youth->last_name }} </b></p style=".'margin-top:10px;'."><br>
                            <p><i>Post: {{ $youth->post }}</i></p><br>
                        </div>
                        @endforeach
                    </div>
                </div>
            </section>
        </div>
    </section>
</div>
<div id="scs">
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle w3-center">Sunday Church School</h2>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container">
            <section class="features">
                <div class="container">
                    <div class="row w3-center">
                        @foreach ($scss as $scs)
                        <div class='col-md-3 w3-center'>
                            <img src='@if($scs->gender == 0) {{ asset('images/leaders/male.jpg') }} @else {{ asset('images/leaders/female.jpg
                            ') }} @endif' height='300px' width='300px' class='img-rounded'><br>
                            <p style="margin-top:10px;"><b> {{ $scs->first_name }} {{ $scs->last_name }} </b></p style=".'margin-top:10px;'."><br>
                            <p><i>Post: {{ $scs->post }}</i></p><br>
                        </div>
                        @endforeach
                    </div>
                </div>
            </section>
        </div>
    </section>
</div>
@endsection