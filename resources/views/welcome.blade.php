@extends('layouts.app') 
@section('title', 'Welcome') 
@section('content')
<section class="aboutUs">
    <div class="container-fluid"><img src="images/quakers.jpg" class="img-center" alt="" style="width:100%;" /></div>
</section>
<section id="content">
    <div class="container">
        <section class="features">
            <div class="row">
                <div class="col-md-4">
                    <h2>Who We Are</h2>
                    <p>Friends Church Ngong Road Monthly Meeting is a <a id="impLink" href="localmeetings.php">collection
                                of seven churches</a> founded in 1976 and is situated along Ngong Road in Nairobi.The first
                        Presiding Clerk was Zebeon Kikuyu and his Vice Presiding Clerk David Malenje. It is one of the twenty
                        one monthly meetings that constitute the Nairobi Yearly Meeting and one of the four monthly meetings
                        that forms Langata Quarterly Coordinating Committee.<a id="impLink" href="nrmm.php"> Read More... </a>
                </div>
                <div class="col-md-4">
                    <h2>Services</h2>
                    <p>We hold <a href="services.php">a main monthly service</a> on the <a id="impLink" href="https://calendar.google.com/calendar/event?action=TEMPLATE&tmeid=NHVvZW1xdGVob3RlYnJhaTd0a2I0bjhiM2tfMjAxNzA2MTFUMDgzMDAwWiBuZ29uZ3JvYWRtb250aGx5QG0&tmsrc=ngongroadmonthly%40gmail.com"
                            target="_blank">second Sunday of each month</a> at <a id="impLink" href="https://www.google.com/maps/place/Friends+International+Centre,+Ngong+Rd,+Nairobi+City,+Kenya/@-1.3005103,36.7871189,17z/data=!3m1!4b1!4m5!3m4!1s0x182f109bc4842189:0xd973a345ed0893e1!8m2!3d-1.3004682!4d36.7891878"
                            target="_blank">Friends International Centre, Ngong Road, Nairobi</a> from 8:00am to 12:00 noon.
                        <br><br>See <a id="impLink" href="services.php">other services you can
                                attend</a>.</p>
                </div>
                <div class="col-md-4">
                    <h2>Visit Us</h2><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.790571952751!2d36.787118914169255!3d-1.3005102990514548!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f109bc4842189%3A0xd973a345ed0893e1!2sFriends+International+Centre%2C+Ngong+Rd%2C+Nairobi+City%2C+Kenya!5e0!3m2!1sen!2s!4v1489087675502"
                        width="450" height="250" frameborder="0" style="border:0;" class="img-responsive" allowfullscreen></iframe>
                </div>
            </div>
        </section>
    </div>
</section>
@endsection