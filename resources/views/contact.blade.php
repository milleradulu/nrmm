@extends('layouts.app') 
@section('title', 'Contact Us') 
@section('content')
<section id="inner-headline" style="margin-top:5px !important;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="pageTitle">Contact Us</h2>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p> <strong>Friends International Centre</strong><br>Ngong Road, Near Uchumi Hyper<br>Nairobi, Kenya.</p>
                @include('partials.errors')
                <div class="contact-form">
                    {!! Form::open(['url' => '/contact', 'name' => 'sentMessage', 'id' => 'contactForm', 'novalidate']) !!}
                    <h3>Talk to us! Leave a question or comment below.</h3>
                    <p>*Fill in all fields</p>

                    <div class="control-group">
                        <div class="controls">
                            {!! Form::text('name', '', ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Full Name', 'requireddata-validation-required-message'
                            => 'Please enter your name']) !!}
                            <p class="help-block"></p>
                        </div>
                    </div>

                    <div class="control-group">
                        <div class="controls">
                            {!! Form::text('subject', '', ['class' => 'form-control', 'id' => 'subject', 'placeholder' => 'Hello', 'requireddata-validation-required-message'
                            => 'Please enter a subject']) !!}
                            <p class="help-block"></p>
                        </div>
                    </div>

                    <div class="control-group">
                        <div class="controls">
                            {!! Form::email('email', '', ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'mail@mail.com', 'requireddata-validation-required-message'
                            => 'Please enter your email']) !!}
                            <p class="help-block"></p>
                        </div>
                    </div>

                    <div class="control-group">
                        <div class="controls">
                            {!! Form::textarea('message', '', ['class' => 'form-control', 'id' => 'message', 'placeholder' => 'Just saying hi', 'requireddata-validation-required-message'
                            => 'Please enter your message', 'minlength' => '5', 'data-validation-minlength-message' => 'Min
                            5 characters', 'maxlength' => '999', 'style' => 'resize:none']) !!}
                            <p class="help-block"></p>
                        </div>
                    </div>
                    <div id="success"> </div>
                    <div class="row w3-margin-top">
                        <div>
                            {!! Form::submit('Message Us!', ['class' => 'btn btn-success wow fadeInUp pull-right']) !!}
                        </div>
                    </div>

                    {!! Form::close() !!}

                </div>
            </div>
            <div class="col-md-6 embed-responsive embed-responsive-16by9" style="margin:opx;"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15955.162289701564!2d36.789308!3d-1.30051!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f109bc4842189%3A0xd973a345ed0893e1!2sFriends+International+Centre%2C+Ngong+Rd%2C+Nairobi+City%2C+Kenya!5e0!3m2!1sen!2sus!4v1494675261293"
                    width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> </div>
        </div>
    </div>
</section>
@endsection