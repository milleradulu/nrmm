@extends('layouts.app') 
@section('title', 'Services') 
@section('content')
<div id="mainservice">
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle w3-center">Services</h2>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container">
            <section class="features">
                <div class="container">
                    <div class="row w3-center">
                        <h1 class="w3-jumbo w3-animate-zoom">COMING SOON</h1>
                    </div>
                </div>
            </section>
        </div>
    </section>
</div>
@endsection