@extends('layouts.app') 
@section('title', 'Ministries') 
@section('content')
<div id="fcdi">
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle w3-center">Friends Children Development Initiative (FCDI)</h2>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container">
            <section class="features">
                <div class="container">
                    <div class="row w3-centr"> This is charitable organization under <a href="index.php">Friends
                                Church Ngong Road </a> founded in 2004 to care and support the needy, orphaned and vulnerable
                        children.<em><br><br>The Main activities </em> are:
                        <li>providing scholarships to eligible students in high schools</li>
                        <li>to recognize and morally and socially care for the needy widows : <em>James 1:27</em>.</li><br>Volunteering
                        Officials are: Sabina Wangia (patron), Judy Agalo (secretary), John Mikisi ( Finance and Development),
                        Lonah Musungu ( treasurer), Jotham Wanaswa (Assistant treasurer), Beatrice Muyekho (hospitality)
                        and Hastings Ozwara ( human resource).<br>Committee members are: Faith Ngaira , Julia Makunda and
                        Rosemary Wakoli, Ngong Road Monthly Meeting PC and pastoral team, Representations from FIC and all
                        local churches, Hannington Mugalla, Emily Lugano, Beatrice Muyekho (hospitality), Beatrice Mutali
                        ( Vice chair). <br><br>Members are encouraged to donate materially and financially.
                    </div>
                </div>
            </section>
        </div>
    </section>
</div>
{{-- <div id="praise">
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle w3-center">Praise and Worship</h2>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container">
            <section class="features">
                <div class="container">
                    <div class="row w3-center"> </div>
                </div>
            </section>
        </div>
    </section>
</div>
<div id="outreach">
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle w3-center">Outreach and Missions</h2>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container">
            <section class="features">
                <div class="container">
                    <div class="row w3-center"> </div>
                </div>
            </section>
        </div>
    </section>
</div>
<div id="catechism">
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle w3-center">Catechism Classes</h2>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container">
            <section class="features">
                <div class="container">
                    <div class="row w3-center"> </div>
                </div>
            </section>
        </div>
    </section>
</div>
<div id="mercy">
    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="pageTitle w3-center">Mercy Ministry</h2>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container">
            <section class="features">
                <div class="container">
                    <div class="row w3-center"> </div>
                </div>
            </section>
        </div>
    </section>
</div> --}}
@endsection