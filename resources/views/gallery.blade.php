@extends('layouts.app') 
@section('title', 'Gallery') 
@section('content')
<section id="inner-headline" style="margin-top:5px !important;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="pageTitle">Gallery</h2>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div>
                    <h2>A look at past events</h2>
                </div><br>
            </div>
        </div>
        <section id="gallery-1" class="content-block section-wrapper gallery-1">
            <div class="container">
                <div class="row">
                    <div id="isotope-gallery-container">

                        @if (count($events) > 0) @foreach ($events as $event)
                        <div class="col-md-4 col-sm-6 col-xs-12 gallery-item-wrapper nature outside">
                            <div class="gallery-item">
                                <div class="gallery-thumb">
                                    <img src="images/quakers.jpg" class="img-responsive" alt="{{ $event->name }}}">
                                    <div class="image-overlay"></div>
                                    <a href="/gallery/{{ $event->id }}" class="gallery-link"><i class="fa fa-link"></i></a>
                                </div>
                                <div class="gallery-details">
                                    <div class="editContent">
                                        <h5><a href="/gallery/{{ $event->id }}">{{ $event->name }}</a></h5>
                                    </div>
                                    <div class="editContent">
                                        <p>{{ $event->description }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach @else
                        <h4>There are no past events.</h4><img src="images/nope.jpg"></div>
                    @endif
                </div>
            </div>
            {{ $events->links() }}
    </div>
    </section>
    </div>
</section>
@endsection
 
@section('customjs')
<script src="{{ asset('js/gallery.js') }}"></script>
@endsection
 
@section('customcss')
<link rel="stylesheet" href="{{ asset('css/animate.css') }}">
<link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
<link rel="stylesheet" href="{{ asset('css/gallery-1.css') }}">
<link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
@endsection