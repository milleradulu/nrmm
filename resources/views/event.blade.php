@extends('layouts.app') 
@section('title', $event->name) 
@section('content')
<section id="inner-headline" style="margin-top:5px !important;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="pageTitle">{{ $event->name }}</h2>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <div class="container">
        <div class="row w3-center"> <img class="img-responsive" src="{{ Voyager::image($event->poster) }}">
            <p>
                <h3>Description</h3>
                {{ $event->description }}
            </p>

            <p>
                <h3>Venue</h3>
                {{ $event->venue }}
            </p>
            <div class="col-sm-6">
                <p>
                    <h3>Start Date</h3>
                    {{ $event->start_date->toFormattedDateString() }}
                </p>
            </div>
            <div class="col-sm-6">
                <p>
                    <h3>Start Time</h3>
                    {{ $event->start_time->toTimeString() }}
                </p>

            </div>

            <div class="col-sm-6">
                <p>
                    <h3>End Date</h3>
                    {{ $event->end_date->toFormattedDateString() }}
                </p>
            </div>
            <div class="col-sm-6">
                <p>
                    <h3>End Time</h3>
                    {{ $event->end_time->toTimeString() }}
                </p>
            </div>

        </div>
    </div>
</section>
@endsection