@extends('layouts.app') 
@section('title', 'Events') 
@section('content')
<section id="inner-headline" style="margin-top:5px !important;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="pageTitle">Events</h2>
            </div>
        </div>
    </div>
</section>
<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div>
                    <h2>See What's up and coming for the Ngong Road Monthly Meeting</h2>
                </div><br>
            </div>
        </div>
        <section id="inner-headline">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="pageTitle w3-center">Today's Events</h2>
                    </div>
                </div>
            </div>
        </section>
        <div class="row">
            @if (count($tevents) > 0) @foreach ($tevents as $tevent)

            <div class="col-lg-3">
                <div class="pricing-box-item">
                    <div class="pricing-heading">
                        <h3><strong><a href="/events/{{$tevent->id}}">{{ $tevent->name }}</a></strong></h3>
                    </div>
                    <div class="pricing-container">
                        <ul>
                            <li><i class="icon-ok"></i> Description :<br> {{ $tevent->description}} </li>
                            <li><i class="icon-ok"></i> Date : {{ Carbon\Carbon::createFromFormat('Y-m-d', $tevent->start_date)->toFormattedDateString() }}</li>
                            <li><i class="icon-ok"></i> Venue : {{ $tevent->venue }}</li>
                        </ul>
                    </div>
                </div>
            </div>

            @endforeach @else
            <div style="background-color:white;" class="w3-center">
                <h4>There are no events today.</h4><img src="images/nope.jpg"></div>
            @endif {{ $tevents->links() }}
        </div>
        <section id="inner-headline">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="pageTitle w3-center">Upcoming Events</h2>
                    </div>
                </div>
            </div>
        </section>
        <div class="row">

            @if (count($uevents) > 0) @foreach ($uevents as $uevent)

            <div class="col-lg-3">
                <div class="pricing-box-item">
                    <div class="pricing-heading">
                        <h3><strong><a href="/events/{{$uevent->id}}">{{ $uevent->name }}</a></strong></h3>
                    </div>
                    <div class="pricing-container">
                        <ul>
                        <li><i class="icon-ok"></i> Description :<br> {{ $uevent->description }}</li>
                            <li><i class="icon-ok"></i> Date : {{ Carbon\Carbon::createFromFormat('Y-m-d', $uevent->start_date)->toFormattedDateString() }} </li>
                            <li><i class="icon-ok"></i> Venue : {{ $uevent->venue }}</li>
                        </ul>
                    </div>
                </div>
            </div>

            @endforeach @else
            <div style="background-color:white;" class="w3-center">
                <h4>There are currently no upcoming events.</h4><img src="images/nope.jpg"></div>
            @endif {{ $uevents->links() }}
        </div>
        <section id="inner-headline">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="pageTitle w3-center">Past Events Events</h2>
                    </div>
                </div>
            </div>
        </section>
        <div class="row">

            @if (count($pevents) > 0) @foreach ($pevents as $pevent)

            <div class="col-lg-3">
                <div class="pricing-box-item">
                    <div class="pricing-heading">
                        <h3><strong><a href="/events/{{ $pevent->id }}">{{ $pevent->name }}</a></strong></h3>
                    </div>
                    <div class="pricing-container">
                        <ul>
                            <li><i class="icon-ok"></i> {{ $pevent->description}} </li>
                            <li><i class="icon-ok"></i> Date : {{ Carbon\Carbon::createFromFormat('Y-m-d', $pevent->start_date)->toFormattedDateString() }}</li>
                            <li><i class="icon-ok"></i> Venue : {{ $pevent->venue }}</li>
                        </ul>
                    </div>
                </div>
            </div>

            @endforeach @else
            <div style="background-color:white;" class="w3-center">
                <h4>There are currently no past events.</h4><img src="images/nope.jpg"></div>
            @endif {{ $pevents->links() }}
        </div>
    </div>
</section>
@endsection