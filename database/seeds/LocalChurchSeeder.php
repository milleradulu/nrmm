<?php

use Illuminate\Database\Seeder;

class LocalChurchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\LocalChurch', 50)->create();
    }
}
