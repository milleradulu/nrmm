<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(LocalChurchSeeder::class);
        $this->call(PastorSeeder::class);
        $this->call(LeaderSeeder::class);
        $this->call(ChurchEventSeeder::class);
    }
}
