<?php

use Illuminate\Database\Seeder;

class PastorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Pastor', 15)->create();
    }
}
