<?php

use Illuminate\Database\Seeder;

class ChurchEventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\ChurchEvent', 50)->create();
    }
}
