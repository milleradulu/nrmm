<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 20);
            $table->string('last_name', 20);
            $table->enum('office', ['main', 'quakermen', 'usfw', 'youth', 'scs']);
            $table->string('post');
            $table->string('photo')->nullable();
            $table->boolean('gender');
            $table->boolean('active');
            $table->unsignedInteger('local_church_id')->nullable();
            $table->timestamps();

            $table->foreign('local_church_id')->references('id')->on('local_churches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaders');
    }
}
