<?php

use Faker\Generator as Faker;

$factory->define(App\Leader::class, function (Faker $faker) {
    $stations = App\LocalChurch::all();
    return [
        'first_name' => $faker->firstNameFemale,
        'last_name' => $faker->lastName,
        'office' => $faker->randomElement(['main', 'quakermen', 'usfw', 'youth', 'scs']),
        'post' => $faker->jobTitle,
        'photo' => $faker->imageUrl(640, 480),
        'gender' => $faker->boolean,
        'active' => $faker->boolean,
        'local_church_id' => $faker->randomElement($stations->pluck('id')->toArray()),
    ];
});
