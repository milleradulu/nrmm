<?php

use Faker\Generator as Faker;

$factory->define(App\LocalChurch::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'location' => $faker->streetAddress,
        'history' => $faker->sentence,
    ];
});
