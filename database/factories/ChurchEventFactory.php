<?php

use Faker\Generator as Faker;

$factory->define(App\ChurchEvent::class, function (Faker $faker) {
    return [
        'slug' => $faker->word,
        'name' => $faker->email,
        'venue' => $faker->streetAddress,
        'description' => $faker->sentence,
        'start_date' => $faker->date('Y:m:d'),
        'end_date' => $faker->date('Y:m:d'),
        'start_time' => $faker->time('H:i:s'),
        'end_time' => $faker->time('H:i:s'),
        'featured_image' => $faker->imageUrl(640, 480),
        'poster' => $faker->imageUrl(640, 480),
    ];
});
