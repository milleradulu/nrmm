<?php

use Faker\Generator as Faker;

$factory->define(App\Pastor::class, function (Faker $faker) {

    $stations = App\LocalChurch::all();
    return [
        'first_name' => $faker->firstNameMale,
        'last_name' => $faker->lastName,
        'photo' => $faker->imageUrl(640, 480),
        'gender' => $faker->boolean,
        'active' => $faker->boolean,
        'local_church_id' => $faker->randomElement($stations->pluck('id')->toArray()),
    ];
});
