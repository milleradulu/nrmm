<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@homePage');
Route::get('/services', 'PageController@servicesPage');
Route::get('/silentmeeting', 'PageController@silentMeetingPage');
Route::get('/leadership', 'LeaderController@index');
Route::get('/pastors', 'PastorController@index');
Route::get('/gallery', 'ChurchEventController@gallery');
Route::get('/events', 'ChurchEventController@index');
Route::get('/ministries', 'PageController@ministriesPage');
Route::get('/localmeetings', 'LocalChurchController@index');
Route::get('/contact', 'ContactController@create');
Route::post('/contact', 'ContactController@store');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/events/{event}', 'ChurchEventController@show');

Route::get('/gallery/{gallery}', 'ChurchEventController@eventGallery');

// Route::get('/posters', 'PageController@getPosters');